package ejemplos.polimorfismo;

/**
 * Clase B
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class B extends A {
    
    public void p() {
        System.out.println("B.p()");
    }    
}
package ejemplos.interfaces;

/**
 * Interfaz para una Pila de Integers
 * @author Derlis
 */
public interface InterfazPila {
    
    public void apilar(int nro);
    
    public int desapilar();
    
    public boolean esPilaLlena();
    
    public boolean esPilaVacia();
    
    public int getTamanhoActualPila();
    
}

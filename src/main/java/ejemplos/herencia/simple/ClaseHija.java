package ejemplos.herencia.simple;

/**
 * Clase ClaseHija
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class ClaseHija extends ClasePadre {

    public void metodoPublicoEnHijo() {
        System.out.println("Metodo publico en clase hija");
    }
    
    public ClaseHija() {
        System.out.println("Constructor vacio en hija");        
    }
    
    //Redefinamos el constructor
    public ClaseHija(String nombreClase) {
        super(nombreClase);
        System.out.println("Constructor en claseHija de clase " + nombreClase);
    }

    //Redefinamos otros metodos
    @Override
    public int calcularSumaRedefinible(int a, int b) {
        System.out.println("Calculo de suma en Hijo...");
        return a+b;
    }
    
    //Sobrecarguemos otros metodos
    public int calcularSumaNoRedefinible(int a, int b, int c) {
        System.out.println("Calculo de suma en Hijo...");
        return a+b;    
    }
} 
package ejemplos.abstractas;

/**
 * Clase ProfesorEscalafonado
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class ProfesorEscalafonado extends Profesor {
    
    //Tiene todo lo de un Profesor y horas extras
    private int horasExtras;
    
    public ProfesorEscalafonado() {
        super();
        horasExtras = 0;
    }
    
    public ProfesorEscalafonado(String n, String d, int e, int a, int sb, int horasEx) {
        super(n, d, e, a, sb);
        horasExtras = horasEx;
    }

    @Override
    public void imprimir() {
        System.out.println("Impresion de profesor escalafonado");
    }

    /**
     * @return the horasExtras
     */
    public int getHorasExtras() {
        return horasExtras;
    }

    /**
     * @param horasExtras the horasExtras to set
     */
    public void setHorasExtras(int horasExtras) {
        this.horasExtras = horasExtras;
    }

} //Fin de clase ProfesorEscalafonado

package ejemplos.abstractas;

/**
 * Clase Profesor
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public abstract class Profesor {

    private String nombre;
    private String direccion;
    private int edad;
    private int antiguedad;
    private int salarioBase;
    
    public abstract void imprimir();  
    
    public Profesor() {
        nombre = "";
        direccion = "";
        edad = 0;
        antiguedad = 0;
        salarioBase = 0;
    }
    
    public Profesor(String n, String d, int e, int a, int sb) {
        nombre = n;
        direccion = d;
        edad = e;
        antiguedad = a;
        salarioBase = sb;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the edad
     */
    public int getEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * @return the antiguedad
     */
    public int getAntiguedad() {
        return antiguedad;
    }

    /**
     * @param antiguedad the antiguedad to set
     */
    public void setAntiguedad(int antiguedad) {
        this.antiguedad = antiguedad;
    }

    /**
     * @return the salarioBase
     */
    public int getSalarioBase() {
        return salarioBase;
    }

    /**
     * @param salarioBase the salarioBase to set
     */
    public void setSalarioBase(int salarioBase) {
        this.salarioBase = salarioBase;
    }
    
    
    
} //Fin de clase Profesor

